//
//  FirstViewController.swift
//  Guesser!
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuesserViewController: UIViewController {
    
    
    
    @IBOutlet weak var myGuessTF: UITextField!
    @IBOutlet weak var resultLBL: UILabel!
    @IBAction func checkBTN(_ sender: Any) {
        if let num = Int(myGuessTF.text!) {
            let output = Guesser.shared.amIRight(guess: Int(num))
            if output == .correct{
                resultLBL.text = output.rawValue
                displayMessage()
                Guesser.shared.createNewProblem()
            }
            else if num < 1 || num > 10{
                errorMessage()
            }
            else{
                resultLBL.text = output.rawValue
            }
            
        }
        else{
            errorMessage()
        }
    }
    
    @IBAction func createProblemBTN(_ sender: Any) {
    resultLBL.text = ""
    myGuessTF.text = ""
        Guesser.shared.createNewProblem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Guesser.shared.createNewProblem()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    func displayMessage(){
        let alert = UIAlertController(title: "Well done",message: "You got it in \(Guesser.shared.numAttempts) tries",preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func errorMessage(){
        let alert = UIAlertController(title: "Error",message: "Please enter a number between 1 - 10",preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
    




