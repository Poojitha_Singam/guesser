//
//  Guesser.swift
//  Guesser!
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
class Guesser {
    private var correctAnswer:Int = 0
    private var _numAttempts:Int
    private var guesses:[Guess]
    public var numAttempts:Int{
        return _numAttempts
    }
    private init(){
        self.correctAnswer = 0
        self._numAttempts = 0
        self.guesses = []
    }
    struct Guess{
        var correctAnswer:Int
        var numAttemptsRequired:Int
    }
    static var shared = Guesser()
    
    func createNewProblem(){
        correctAnswer = Int.random(in: 1...10)
        _numAttempts = 0
    }
    enum Result:String {
        case tooLow = "Too Low"
        case tooHigh = "Too High"
        case correct = "Correct"
    }
    func amIRight(guess:Int) -> Result{
        _numAttempts = _numAttempts + 1
        if guess > correctAnswer{
            return .tooHigh
        }else{
            if guess < correctAnswer{
                return .tooLow
            }else{
                guesses.append(Guess(correctAnswer: correctAnswer, numAttemptsRequired: _numAttempts))
           
                return .correct
            }
        }
    }
    func guess(index: Int) -> Guess{
        return guesses[index]
    }
    subscript(guess:Int) -> Guess{
        return guesses[guess]
    }
    func numGuesses() -> Int{
        return guesses.count
    }
    func clearStatistics(){
        guesses = []
    }
    
    func minNumAttempts() -> Int{
        if guesses.count != 0{
        var minimum = guesses[0].numAttemptsRequired
        
        for i in guesses{
            if i.numAttemptsRequired < minimum{
                minimum = i.numAttemptsRequired
            }
    }
            return minimum}
        else{
            return 0
        }
    }
    func maxNumAttempts() -> Int{
        if guesses.count != 0{
        var maximum = guesses[0].numAttemptsRequired
        for i in guesses{
            if i.numAttemptsRequired > maximum{
                maximum = i.numAttemptsRequired
            }
        }
            return maximum}
        else{
            return 0
        }
    }
    
}

