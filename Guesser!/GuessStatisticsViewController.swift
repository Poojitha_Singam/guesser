//
//  ThirdViewController.swift
//  Guesser!
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuessStatisticsViewController: UIViewController {

    @IBOutlet weak var minLBL: UILabel!
    @IBOutlet weak var maxLBL: UILabel!
    @IBOutlet weak var meanLBL: UILabel!
    @IBOutlet weak var stdDevLBL: UILabel!
    
    @IBAction func clearStatisticsBTN(_ sender: Any) {
        minLBL.text = "0"
        maxLBL.text = "0"
        meanLBL.text = "0.0"
        stdDevLBL.text = "0.0"
        Guesser.shared.clearStatistics()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        constraints()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
     constraints()
   }
    func constraints(){
        minLBL.text = String(Guesser.shared.minNumAttempts())
        maxLBL.text = String(Guesser.shared.maxNumAttempts())
        var sum = 0
        for i in 0..<Guesser.shared.numGuesses(){
            sum = sum + Guesser.shared.guess(index: i).numAttemptsRequired
        }
        let mean = Double(sum)/Double(Guesser.shared.numGuesses())
        meanLBL.text = "\(mean)"
        var stdsum = 0.0
        for i in 0..<Guesser.shared.numGuesses(){
            stdsum = stdsum + pow(Double(Guesser.shared.guess(index: i).numAttemptsRequired) - mean , 2)
        }
        stdDevLBL.text = "\(stdsum/Double(Guesser.shared.numGuesses()))"
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
